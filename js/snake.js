(function(){

  if (typeof Snake === "undefined") {
    window.Snake = {};
  }

  var Snake = window.Snake.Snake = function(){
    this.POSSIBLE_DIRECTIONS = ["N", "S", "E", "W"];
    this.segments = [];
    this.dir = this.POSSIBLE_DIRECTIONS[0];
    this.position = new Coordinate(10,10);

  };
  Snake.prototype.move = function () {
    switch (this.dir) {
      case "N":
        this.position.move(0,1);
        break;
      case "S"
        this.position.move(0,-1);
        break;
      case "E":
        this.position.move(1,0);
        break;
      case "W":
        this.position.move(-1,0);
        break;
      default:
        console.log("Error");
    }
  }

  Snake.prototype.turn = function(direction) {
    this.dir = direction;
  }

})();
