(function(){

  if (typeof Snake === "undefined") {
    window.Snake = {};
  }

  var Coordinate = window.Snake.Coordinate = function(x, y){
    this.x = x;
    this.y = y;
  };

  Coordinate.prototype.plus = function(otherCoordinate) {
    return new Coordinate(this.x + otherCoordinate.x, this.y + otherCoordinate.y);
  };

  Coordinate.prototype.move = function(x, y) {
    this.x += x;
    this.y += y;
  };

  Coordinate.prototype.equals = function(otherCoordinate) {
    return (this.x === otherCoordinate.x && this.y === otherCoordinate.y);
  };

  Coordinate.prototype.isOpposite = function(otherCoordinate) {

  };

})();
